# https://python-gitlab.readthedocs.io/en/stable/api-usage.html
import gitlab
import os
import argparse

parser = argparse.ArgumentParser(description='Create GitLab Merge request')
parser.add_argument('project_name',
                    type=str, 
                    help='demos/infrastructure/merge-request-creator')
parser.add_argument('source_branch', 
                    type=str, 
                    help='main',
                    default='main')
parser.add_argument('target_branch', 
                    type=str, 
                    help='feature/add-button')
parser.add_argument('title', 
                    type=str, 
                    help='Auto Generated Merge Request')
args = parser.parse_args()

project_name_with_namespace = args.project_name
source_branch = args.source_branch
target_branch = args.target_branch
title = args.title

# TO DO make GLPAT an argparse
glpat = os.environ['GLPAT']

gl = gitlab.Gitlab(private_token=glpat)
# SELF_HOSTED gl = gitlab.Gitlab(url='https://gitlab.example.com', private_token='JVNSESs8EwWRx5yDxM5q')
try:
    # Get a project by name with namespace
    project = gl.projects.get(project_name_with_namespace)
    print("Project successfuly found")
    # create branch
    branch = project.branches.create({'branch': source_branch, 'ref': 'main'})
    print("Branch successfully created")
    # open merge request
    project.mergerequests.create({'source_branch': source_branch,
                                    'target_branch': target_branch,
                                    'title': title})
    print("Merge request successfully opened")
except Exception as error:
    print("Error:", error)